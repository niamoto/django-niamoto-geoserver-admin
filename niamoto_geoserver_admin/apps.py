# coding: utf-8

from django.apps import AppConfig


class NiamotoGeoserverAdminConfig(AppConfig):
    name = 'niamoto_geoserver_admin'
