=====
django-niamoto-geoserver-admin
=====

Django reusable application providing models and utilities to manage the niamoto
geoserver instance.

Detailed documentation is in the "docs" directory.

Quick start
-----------

1. Add "niamoto_geoserver_admin" to your INSTALLED_APPS setting like this::

    INSTALLED_APPS = [
        ...
        'niamoto_geoserver_admin',
    ]

2. Run `python manage.py migrate` to create the niamoto_geoserver_admin models.

